package ru.t1.lazareva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.IProjectRepository;
import ru.t1.lazareva.tm.api.repository.ITaskRepository;
import ru.t1.lazareva.tm.api.repository.IUserRepository;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.api.service.IUserService;
import ru.t1.lazareva.tm.enumerated.Role;
import ru.t1.lazareva.tm.exception.entity.UserNotFoundException;
import ru.t1.lazareva.tm.exception.field.EmailEmptyException;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.exception.field.LoginEmptyException;
import ru.t1.lazareva.tm.exception.field.PasswordEmptyException;
import ru.t1.lazareva.tm.exception.user.ExistsEmailException;
import ru.t1.lazareva.tm.exception.user.ExistsLoginException;
import ru.t1.lazareva.tm.exception.user.RoleEmptyException;
import ru.t1.lazareva.tm.model.User;
import ru.t1.lazareva.tm.util.HashUtil;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ITaskRepository taskRepository;

    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserRepository userRepository,
            @NotNull final ITaskRepository taskRepository,
            @NotNull final IProjectRepository projectRepository
    ) {
        super(userRepository);
        this.propertyService = propertyService;
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        return repository.create(propertyService, login, password);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.create(propertyService, login, password, email);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @Nullable final User user = create(login, password);
        user.setRole(role);
        return repository.create(propertyService, login, password, role);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = repository.findByLogin(login);
        return user;
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = repository.findByEmail(email);
        return user;
    }

    @Nullable
    @Override
    public User remove(@Nullable final User model) {
        if (model == null) return null;
        @Nullable final User user = super.remove(model);
        if (user == null) return null;
        @NotNull final String userId = user.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        return this.remove(user);
    }

    @Nullable
    @Override
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        return this.remove(user);
    }

    @NotNull
    @Override
    public User setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @NotNull
    @Override
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user != null) {
            user.setLocked(true);
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user != null) {
            user.setLocked(false);
        }
    }

}