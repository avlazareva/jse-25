package ru.t1.lazareva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.IUserOwnedRepository;
import ru.t1.lazareva.tm.api.service.IUserOwnedService;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.exception.field.IdEmptyException;
import ru.t1.lazareva.tm.exception.field.IndexIncorrectException;
import ru.t1.lazareva.tm.exception.field.UserIdEmptyException;
import ru.t1.lazareva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>> extends AbstractService<M, R> implements IUserOwnedService<M>
 {

    public AbstractUserOwnedService(final R repository) {
        super(repository);
    }

     @Override
     public void clear(@Nullable String userId) {
         if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
         repository.clear(userId);
     }

     @Override
     public boolean existsById(@Nullable String userId, @Nullable String id) {
         if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
         if (id == null || id.isEmpty()) throw new IdEmptyException();
         return repository.existsById(userId, id);
     }

     @NotNull
     @Override
     public List<M> findAll(@Nullable String userId) {
         if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
         return repository.findAll(userId);
     }

     @NotNull
     @Override
     public List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator) {
         if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
         if (comparator == null) return repository.findAll(userId);
         return repository.findAll(userId, comparator);
     }

     @Nullable
     @Override
     public M findOneById(@Nullable String userId, @Nullable String id) {
         if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
         if (id == null || id.isEmpty()) throw new IdEmptyException();
         return repository.findOneById(userId, id);
     }

     @Nullable
     @Override
     public M findOneByIndex(@Nullable String userId, @Nullable Integer index) {
         if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
         if (index == null) throw new IndexIncorrectException();
         return repository.findOneByIndex(userId, index);
     }

     @Override
     public int getSize(@Nullable String userId) {
         if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
         return repository.getSize(userId);
     }

     @Nullable
     @Override
     public M removeById(@Nullable String userId, @Nullable String id) {
         if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
         if (id == null || id.isEmpty()) throw new IdEmptyException();
         return repository.removeById(userId, id);
     }

     @Nullable
     @Override
     public M removeByIndex(@Nullable String userId, @Nullable Integer index) {
         if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
         if (index == null) throw new IndexIncorrectException();
         return repository.removeByIndex(userId, index);
     }

     @Nullable
     @Override
     public M add(@Nullable String userId, @Nullable M model) {
         if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
         if (model == null) return null;
         return repository.add(userId, model);
     }

     @Nullable
     @Override
     public M remove(@Nullable String userId, @Nullable M model) {
         if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
         if (model == null) return null;
         return repository.remove(userId, model);
     }

     @NotNull
     @SuppressWarnings("unchecked")
     @Override
     public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
         if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
         if (sort == null) return findAll(userId);
         return repository.findAll(userId, sort.getComparator());
     }

 }